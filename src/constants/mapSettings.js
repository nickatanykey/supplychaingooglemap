const mapSettings = {
    clickableIcons: false,
    streetViewControl: true,
    panControlOptions: false,
    gestureHandling: "cooperative",
    mapTypeControl: true, 
    mapTypeID: "roadmap",
    zoomControlOptions: {
      style: "SMALL"
    },
    zoom: 5,
    minZoom: 2,
    maxZoom: 8,
};

const markerIcon = {
    "red": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
    "blue": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
    "orange": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
    "green": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
    "purple": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
    "pink": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
    "ltblue": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
    "yellow": {
        url:"http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }, 
}

export {mapSettings, markerIcon}